﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony("NCP");
            //Harmony.DEBUG = true;
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}
