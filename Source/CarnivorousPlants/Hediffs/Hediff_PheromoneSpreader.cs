﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using RimVore2;
using RimWorld;

namespace CarnivorousPlants
{
    class Hediff_PheromoneSpreader : Hediff
    {
        const bool useCellCalculations = false;
        const float effectRadius = 10f;
        bool isInactive = false;
        bool moteActive = false;
        float currentMoteRotation = 0f;
#if !v1_2
        FleckCreationData moteData;
#endif

        public override void Tick()
        {
            base.Tick();
            if (base.pawn?.Dead != false)
            {
                moteActive = false;
#if !v1_2
                SustainMote();
#endif
                return;
            }
            bool currentlyInactive = GlobalVoreTrackerUtility.IsTrackingVore(base.pawn);

            // if currently voring, don't spread pheromones
            if (currentlyInactive)
            {
                moteActive = false;
#if !v1_2
                SustainMote();
#endif
                return;
            }
            IEnumerable<Pawn> affectedPawns = base.pawn.Map.mapPawns.AllPawnsSpawned
                    .Where(p => p.Position.DistanceTo(pawn.Position) < effectRadius
                        && !p.IsMechanoid()
                        //&& (this.pawn.Faction != null && p.Faction != this.pawn.Faction)
                    );

            foreach(Pawn pawn in affectedPawns)
            {
                IncreaseLureHediffSeverity(pawn);
            }
            moteActive = true;
#if !v1_2
            SustainMote();
#endif
        }

#if !v1_2
        private void SustainMote()
        {
            if (!base.pawn.IsHashIntervalTick(Common.MoteLifetimeTicks))
            {
                return;
            }
            if(!moteActive)
            {
                return;
            }
            Map map = base.pawn.Map;
            moteData = FleckMaker.GetDataStatic(base.pawn.DrawPos, base.pawn.Map, Common.PheromonesMote);
            currentMoteRotation += Common.PheromonesMote.solidTime * Common.MoteRotationPerSecond;
            moteData.rotation = currentMoteRotation;
            moteData.rotationRate = Common.MoteRotationPerSecond;
            moteData.scale = effectRadius * 4;
            map.flecks.CreateFleck(moteData);
        }
#endif

        private void IncreaseLureHediffSeverity(Pawn pawn)
        {
            if (pawn.health?.hediffSet == null)
            {
                return;
            }
            Hediff hediff;
            // don't apply to spreaders of the pheromones
            if (pawn.health.hediffSet.HasHediff(Common.SpreaderHediff))
            {
                return;
            }
            // if no pheromone hediff applied yet, apply it
            if (!pawn.health.hediffSet.HasHediff(Common.LureHediff))
            {
                hediff = pawn.health.AddHediff(Common.LureHediff);
            }
            // otherwise just get the existing hediff
            else
            {
                hediff = pawn.health.hediffSet.hediffs.Find(hed => hed.def == Common.LureHediff);
            }
            hediff.Severity += 0.001f;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref isInactive, "currentlyInactive");
        }
    }
}
