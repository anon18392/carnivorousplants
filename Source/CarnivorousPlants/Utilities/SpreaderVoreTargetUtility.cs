﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace CarnivorousPlants
{
    public static class SpreaderVoreTargetUtility
    {
        public static bool SpreaderCanVoreTarget(Pawn spreadingPawn, Pawn target)
        {
            if (spreadingPawn == null || target == null)
                return false;
            if (spreadingPawn.Downed)
                return false;
            if (target.Dead)
                return false;
            if (!spreadingPawn.health.hediffSet.HasHediff(Common.SpreaderHediff))
                return false;
            if (!target.CanReserveAndReach(spreadingPawn, PathEndMode.ClosestTouch, Danger.Deadly))
                return false;

            List<RV2DesignationDef> designationWhitelist = new List<RV2DesignationDef>() { RV2DesignationDefOf.fatal };
            VoreInteractionRequest request = new VoreInteractionRequest(spreadingPawn, target, VoreRole.Predator, designationWhitelist: designationWhitelist);
            VoreInteraction interaction = VoreInteractionManager.Retrieve(request);
            if (!interaction.IsValid)
                return false;

            return true;    
        }
    }
}
