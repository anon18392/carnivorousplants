﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using Verse;

//namespace CarnivorousPlants
//{
//    public static class PheromoneDrawUtility
//    {
//        private static Dictionary<Pawn, float> pheromoneSourceLocations = new Dictionary<Pawn, float>();
//        private static Color PheromoneColor = new Color(204, 84, 84);

//        public static void Register(Pawn pawn, float radius)
//        {
//            if (pheromoneSourceLocations.ContainsKey(pawn))
//            {
//                return;
//            }
//            pheromoneSourceLocations.Add(pawn, radius);
//        }

//        public static void UnRegister(Pawn pawn)
//        {
//            if (!pheromoneSourceLocations.ContainsKey(pawn))
//            {
//                return;
//            }
//            pheromoneSourceLocations.Remove(pawn);
//        }

//        public static void DrawTextures()
//        {
//            IEnumerable<KeyValuePair<Pawn, float>> mapLocations = pheromoneSourceLocations
//                .Where(kvp => !kvp.Key.Dead
//                && kvp.Key.Map == Find.CurrentMap); // don't draw pawns that are not on the current map (however the fuck this could happen with vore plants)
//            foreach(KeyValuePair<Pawn, float> mapLocation in mapLocations)
//            {
//                Pawn pawn = mapLocation.Key;
//                float radius = mapLocation.Value;
//                IntVec3 pos = pawn.Position;
//                GenDraw.DrawRadiusRing(pos, radius, PheromoneColor);
//            }
//        }
//    }
//}
