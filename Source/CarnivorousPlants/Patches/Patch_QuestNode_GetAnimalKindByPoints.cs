﻿using HarmonyLib;
using RimWorld.QuestGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    [HarmonyPatch(typeof(QuestNode_GetAnimalKindByPoints), "SetVars")]
    public class Patch_QuestNode_GetAnimalKindByPoints
    {
        [HarmonyPostfix]
        private static void RerollIfCarnivorousPlant(ref bool __result, Slate slate)
        {
            if (!__result)
            {
                // base game has no pawnKindDef, so we skip
                return;
            }
            PawnKindDef currentPawnKind = slate.Get<PawnKindDef>("animalKindDef");
            if (!currentPawnKind.HasModExtension<Extension_CarnivorousPlantFlag>())
            {
                // not a carnivorous plant, nothing to do
                return;
            }
            float points = slate.Get<float>("points", 0f, false);
            PawnKindDef newPawnKind = DefDatabase<PawnKindDef>.AllDefsListForReading
                .Where(pawnKind => pawnKind.RaceProps.Animal
#if !v1_2
                    && !pawnKind.RaceProps.Dryad
#endif
                    && pawnKind.combatPower < points
                    && !pawnKind.HasModExtension<Extension_CarnivorousPlantFlag>()
                ).RandomElementWithFallback();
            if(newPawnKind == null)
            {
                Log.Message("Base game tried to use a carnivorous plant for a quest, we tried to intercept and look for a different pawnKind, but there were none, so we are skipping this quest");
                __result = false;
            }
            Log.Message("Carnivorous plant was prevented from being used in a quest, used " + newPawnKind + " instead");
            slate.Set("animalKindDef", newPawnKind);
        }
    }
}
