﻿using RimVore2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace CarnivorousPlants
{
    public class MentalStateWorker_Lure : MentalStateWorker
    {
        public MentalStateDef VoreMentalStateDef => (MentalStateDef)base.def;

        public override bool StateCanOccur(Pawn pawn)
        {
            if (!base.StateCanOccur(pawn))
            {
                return false;
            }
            return pawn.Map.mapPawns.AllPawnsSpawned
                .Any(p => SpreaderVoreTargetUtility.SpreaderCanVoreTarget(p, pawn));
        }
    }
}
