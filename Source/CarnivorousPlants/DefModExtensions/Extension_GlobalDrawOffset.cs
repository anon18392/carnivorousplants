﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace CarnivorousPlants
{
    internal class Extension_GlobalDrawOffset : DefModExtension
    {
		private Vector3? drawOffset;
		private Vector3? drawOffsetNorth;
		private Vector3? drawOffsetEast;
		private Vector3? drawOffsetSouth;
		private Vector3? drawOffsetWest;

		/// <summary>
		/// These all use the same approach, try to use a directional offset first, if that does not exist, use the global one, if that also doesn't exit, use a no offset
		/// </summary>
		/// <param name="rot"></param>
		/// <returns></returns>
		public Vector3 DrawOffsetForRot(Rot4 rot)
		{
			switch (rot.AsInt)
			{
				case 0:
						return drawOffsetNorth ?? drawOffset ?? Vector3.zero;
				case 1:
						return drawOffsetEast ?? drawOffset ?? Vector3.zero;
				case 2:
						return drawOffsetSouth ?? drawOffset ?? Vector3.zero;
				case 3:
						return drawOffsetWest ?? drawOffset ?? Vector3.zero;
				default:
					return drawOffset ?? Vector3.zero;
			}
		}
	}
}
