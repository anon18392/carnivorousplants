﻿using RimVore2;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CarnivorousPlants
{
    public static class Common
    {
        //public static ThingDef pheromoneGas = ThingDef.Named("NCP_Gas_Pheromone");
        public static HediffDef SpreaderHediff = HediffDef.Named("NCP_PheromoneSpreader");
        public static HediffDef LureHediff = HediffDef.Named("NCP_PheromoneLure");
        //public static JobDef StationaryPredator = DefDatabase<JobDef>.GetNamed("NCP_VoreInitAsPredatorStationary");
        //public static JobDef GotoLured = DefDatabase<JobDef>.GetNamed("NCP_GotoLured");
        //public static JobDef SelfDestruct = DefDatabase<JobDef>.GetNamed("NCP_SelfDestruct");
        public static QuirkDef EndoPredOnlyQuirk = DefDatabase<QuirkDef>.GetNamed("Inability_PredEndoOnly");
#if v1_2

#else
        public static FleckDef PheromonesMote = DefDatabase<FleckDef>.GetNamed("NCP_Pheromones");
#endif

        public static MentalStateDef LureState = DefDatabase<MentalStateDef>.GetNamed("NCP_Lured");

#if !v1_2
        private static int moteLifetimeTicks = -1;
        public static int MoteLifetimeTicks
        {
            get
            {
                if(moteLifetimeTicks == -1)
                {
                    moteLifetimeTicks = GenTicks.SecondsToTicks(PheromonesMote.solidTime);
                }
                return moteLifetimeTicks;
            }
        }

        [TweakValue("Carni", 0, 150)]
        private static float moteRotationPerSecond = -1;
        public static float MoteRotationPerSecond
        {
            get
            {
                if(moteRotationPerSecond == -1)
                {
                    moteRotationPerSecond = PheromonesMote.GetModExtension<Extension_FleckDefRotation>().rotationPerSecond;
                }
                return moteRotationPerSecond;
            }
        }
#endif
    }
}